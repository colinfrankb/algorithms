using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Xunit.Abstractions;

namespace Algorithms
{
    public class LongestSubsequenceInTwoStrings
    {
        // Write a function that takes two strings, s1 and s2
        // and returns the longest common subsequence of s1 and s2

        // "ABAZDC", "BACBAD" => "ABAD"
        // "AGGTAB", "GXTXAYB" => "GTAB"
        // "aaaa", "aa" => "aa"
        // "", "..." => ""
        // "ABBA", "ABCABA" => "ABBA"

        private readonly ITestOutputHelper _output;

        public LongestSubsequenceInTwoStrings(ITestOutputHelper output)
        {
            _output = output;
        }

        [Theory]
        [InlineData("ABAZDC", "BACBAD", "ABAD")]
        [InlineData("AGGTAB", "GXTXAYB", "GTAB")]
        [InlineData("aaaa", "aa", "aa")]
        [InlineData("ABBA", "ABCABA", "ABBA")]
        public void ReturnsLongestSubsequence(string x, string y, string expected)
        {
            string longestSubsequence = string.Empty;

            for(int indexOfX = 0; indexOfX < x.Length; indexOfX++)
            {
                string sequence = string.Empty;
                sequence = SearchForSequence(x, indexOfX, y, 0, sequence);
                _output.WriteLine(sequence);
                
                if (sequence.Length > longestSubsequence.Length)
                {
                    longestSubsequence = sequence;
                }
            }

            Assert.Equal(expected, longestSubsequence);
        }

        // "ABAZDC", "BACBAD"
        // "AGGTAB", "GXTXAYB"
        public string SearchForSequence(string x, int xOffset, string y, int yOffset, string sequence)
        {
            for(int indexOfX = xOffset; indexOfX < x.Length; indexOfX++)
            {
                for(int indexOfY = yOffset; indexOfY < y.Length; indexOfY++)
                {
                    if (x[indexOfX] == y[indexOfY])
                    {
                        sequence += y[indexOfY];
                        yOffset = indexOfY + 1;
                        break;
                    }
                }
            }

            return sequence;
        }
    }
}
